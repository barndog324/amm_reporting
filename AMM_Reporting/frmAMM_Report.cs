﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.ReportSource;
using CrystalDecisions.Shared;
using System.Configuration;

namespace AMM_Reporting
{
    public partial class frmAMM_Report : Form
    {
        public int searchMode = 1;
        public string EpicorServer = "";

        public frmAMM_Report()
        {
            InitializeComponent();
        }

        #region Events       
        private void frmAMM_Report_Load(object sender, EventArgs e)
        {
            //populateElectronReport();

            dtpElectronBeginDate.Value = new DateTime(DateTime.Now.Year, 1, 1);
            dtpElectronEndDate.Value = DateTime.Now;           

            populatePickLickGrid();
        }

        private void btnEP_DisplayRpt_Click(object sender, EventArgs e)
        {
            populateElectronReport();
        }

        private void rbElectronByDate_CheckedChanged(object sender, EventArgs e)
        {
            lbElectronBeginDate.Visible = true;
            lbElectronEndDate.Visible = true;
            dtpElectronBeginDate.Visible = true;
            dtpElectronEndDate.Visible = true;
        }

        private void rbElectronByPartNum_CheckedChanged(object sender, EventArgs e)
        {
            lbElectronBeginDate.Visible = false;
            lbElectronEndDate.Visible = false;
            dtpElectronBeginDate.Visible = false;
            dtpElectronEndDate.Visible = false;
        }

        private void btnOAUPartsDisplay_Click(object sender, EventArgs e)
        {
            string reportString = "";

            ReportDocument cryRpt = new ReportDocument();   

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@BeginDate";           
            pdv1.Value = Convert.ToDateTime(this.dtpOAU_PartsBeginDate.Text);
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@EndDate";           
            pdv2.Value = Convert.ToDateTime(this.dtpOAU_PartsEndDate.Text);
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            crvOAUPartsRpt.ParameterFieldInfo = paramFields;


            EpicorServer = ConfigurationManager.AppSettings["EpicorAppServer"];
            reportString = @"\\" + EpicorServer + "\\Apps\\AMM_AuditReporting\\reports\\OAU_ProdPartTransAuditTest.rpt";
           
            //reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdPartTransAudit.rpt";
#if DEBUG
            reportString = @"\\kccwvtepic9app2\Apps\AMM_AuditReporting\reports\OAU_TestPartTransAudit.rpt";             
#endif
            
            cryRpt.Load(reportString);            

            crvOAUPartsRpt.ReportSource = cryRpt;
            crvOAUPartsRpt.Visible = true;                         

            this.Refresh();            
        }


        private void btnOAU_IndividualPartDisplay_Click(object sender, EventArgs e)
        {
            string reportString = "";           

            ReportDocument cryRpt = new ReportDocument();

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@BeginDate";
            pdv1.Value = Convert.ToDateTime(this.dtpIndPartBeginDate.Text);
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@EndDate";
            pdv2.Value = Convert.ToDateTime(this.dtpIndPartEndDate.Text);
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            ParameterField pf3 = new ParameterField();
            ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
            pf3.Name = "@PartNum";
            pdv3.Value = this.cbPartNumber.Text;
            pf3.CurrentValues.Add(pdv3);

            paramFields.Add(pf3);

            crvOAU_IndPartTranRpt.ParameterFieldInfo = paramFields;

            EpicorServer = ConfigurationManager.AppSettings["EpicorAppServer"];
            reportString = @"\\" + EpicorServer + "\\Apps\\AMM_AuditReporting\\reports\\OAU_ProdIndividualPartTransAudit.rpt"; 

            //reportString = @"\\kccwvpepic9app\Apps\AMM_AuditReporting\reports\OAU_ProdIndividualPartTransAudit.rpt";
#if DEBUG
            reportString = @"\\kccwvtepic9app2\Apps\AMM_AuditReporting\reports\OAU_TestIndividualPartTransAudit.rpt";             
#endif

            cryRpt.Load(reportString);

            crvOAU_IndPartTranRpt.ReportSource = cryRpt;
            crvOAU_IndPartTranRpt.Visible = true;

            this.Refresh();           
        }

        private void btnIndUserDisplayRpt_Click(object sender, EventArgs e)
        {
            string reportString = "";
            string userName = "";
            string entryPerson = "";
            string empID = "";
            string name;

            userName = this.cbUserID.SelectedValue.ToString();
            name = this.cbUserID.Text;

            if (userName.Contains("SHOP"))
            {
                entryPerson = "SHOP";
                empID = userName.Substring(5, (userName.Length - 5));
            }
            else
            {
                entryPerson = userName.Substring(0, userName.IndexOf("-"));
                empID = entryPerson;
            }
            ReportDocument cryRpt = new ReportDocument();

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@BeginDate";
            pdv1.Value = Convert.ToDateTime(this.dtpIndUserBeginDate.Text);
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@EndDate";
            pdv2.Value = Convert.ToDateTime(this.dtpIndUserEndDate.Text);
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            ParameterField pf3 = new ParameterField();
            ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
            pf3.Name = "@UserID";
            pdv3.Value = empID;
            pf3.CurrentValues.Add(pdv3);

            paramFields.Add(pf3);

            ParameterField pf4 = new ParameterField();
            ParameterDiscreteValue pdv4 = new ParameterDiscreteValue();
            pf4.Name = "@UserName";
            pdv4.Value = name;
            pf4.CurrentValues.Add(pdv4);

            paramFields.Add(pf4);

            //ParameterField pf5 = new ParameterField();
            //ParameterDiscreteValue pdv5 = new ParameterDiscreteValue();
            //pf5.Name = "@Name";
            //pdv5.Value = name;
            //pf5.CurrentValues.Add(pdv5);

            //paramFields.Add(pf5);

            crvOAU_IndUserTranRpt.ParameterFieldInfo = paramFields;


            EpicorServer = ConfigurationManager.AppSettings["EpicorAppServer"];
            reportString = @"\\" + EpicorServer + "\\Apps\\AMM_AuditReporting\\reports\\OAU_ProdIndividualUserTransAudit.rpt";

            //reportString = @"\\kccwvpepic9app\Apps\AMM_AuditReporting\reports\OAU_ProdIndividualUserTransAudit.rpt";

#if DEBUG
            reportString = @"\\kccwvtepic9app2\Apps\AMM_AuditReporting\reports\OAU_TestIndividualUserTransAudit.rpt";             
#endif

            cryRpt.Load(reportString);

            crvOAU_IndUserTranRpt.ReportSource = cryRpt;
            crvOAU_IndUserTranRpt.Visible = true;

            this.Refresh();           
        }

        private void btnEP_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnOAUPartsExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnOAU_IndividualPartExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnIndUserExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnOAU_MetalTranExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void tabCntrlMain_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tabCntrlMain.SelectedIndex)
            {
                case 2:               
                    {
                        populatePartNumComboIndPartPage();
                        break;
                    }                
                case 3:
                    {
                        setupAPOB_Page();
                        break;
                    }
                case 4:
                    {
                        populateUserIDComboIndUserPage();
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        #endregion

        #region Other Methods

        private void populatePartNumComboIndPartPage()
        {
           
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AMM_Reporting.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.GetPurchasePartList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }

                this.cbPartNumber.DataSource = dt;
                this.cbPartNumber.DisplayMember = "PartNum";
                this.cbPartNumber.ValueMember = "PartNum";

            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }

        private void populateUserIDComboIndUserPage()
        {

            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AMM_Reporting.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.GetUserList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }

                this.cbUserID.DataSource = dt;
                this.cbUserID.DisplayMember = "UserName";
                this.cbUserID.ValueMember = "EntryPerson";

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void populateElectronReport()
        {
            string reportString = "";

            ReportDocument cryRpt = new ReportDocument();
            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@BeginDate";
            pdv1.Value = Convert.ToDateTime(this.dtpElectronBeginDate.Text);
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@EndDate";
            pdv2.Value = Convert.ToDateTime(this.dtpElectronEndDate.Text);
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            //ParameterField pf3 = new ParameterField();
            //ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
            //pf3.Name = "@PartNum";
            //pdv3.Value = this.cbPartNumber.Text;
            //pf3.CurrentValues.Add(pdv3);

            //paramFields.Add(pf3);

            


            if (rbElectronByDate.Checked == true)
            {
                EpicorServer = ConfigurationManager.AppSettings["EpicorAppServer"];
                reportString = @"\\" + EpicorServer + "\\Apps\\AMM_AuditReporting\\reports\\OAU_ProdElectronAMMPartTransAuditByDate.rpt"; 

                //reportString = @"\\kccwvpepic9app\Apps\AMM_AuditReporting\reports\OAU_ProdElectronAMMPartTransAuditByDate.rpt";
                crvElectronRpt.ParameterFieldInfo = paramFields;
#if DEBUG
                reportString = @"\\kccwvtepic9app2\Apps\AMM_AuditReporting\reports\OAU_TestElectronAMMPartTransAuditByDate.rpt";             
#endif

            }
            else
            {
                EpicorServer = ConfigurationManager.AppSettings["EpicorAppServer"];
                reportString = @"\\" + EpicorServer + "\\Apps\\AMM_AuditReporting\\reports\\OAU_ProdElectronAMMPartTransAudit.rpt"; 

                //reportString = @"\\kccwvpepic9app\Apps\AMM_AuditReporting\reports\OAU_ProdElectronAMMPartTransAudit.rpt";
#if DEBUG
                reportString = @"\\kccwvtepic9app2\Apps\AMM_AuditReporting\reports\OAU_TestElectronAMMPartTransAudit.rpt";             
#endif
            }
            cryRpt.Load(reportString);

            crvElectronRpt.ReportSource = cryRpt;
            crvElectronRpt.Visible = true;

            this.Refresh();
            this.crvElectronRpt.Refresh();  

        }

        #endregion

        private void btnOAU_MetalTranDisplay_Click(object sender, EventArgs e)
        {

        }


        #region PickList Tab

        private void populatePickLickGrid()
        {
            int jobStatus = 2;
            string jobNumberStr = "";

            DataTable dt = new DataTable();

            if (this.rbPickListAuditOpenJobs.Checked == true)
            {
                jobStatus = 0;
            }
            else if (this.rbPickListAuditClosedJobs.Checked == true)
            {
                jobStatus = 1;
            }            

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AMM_Reporting.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.GetOAU_PickListMain", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNumSearchStr", jobNumberStr);
                        cmd.Parameters.AddWithValue("@JobStatus", jobStatus);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            dgvPickListMain.DataSource = dt;

        }        

        private void buttonMainExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }        

        private void rbPickListAuditOpenJobs_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rbPickListAuditOpenJobs.Checked == true)
            {
                populatePickLickGrid();
            }
        }

        private void rbPickListAuditClosedJobs_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rbPickListAuditClosedJobs.Checked == true)
            {
                populatePickLickGrid();
            }
        }

        private void rbPickListAuditAllJobs_CheckedChanged(object sender, EventArgs e)
        {
            if (this.rbPickListAuditAllJobs.Checked == true)
            {
                populatePickLickGrid();
            }
        }

        private void btnPickListAuditReset_Click(object sender, EventArgs e)
        {
            dtpPickListDate.Value = DateTime.Today;
        }

        private void btnPickListAuditDisplay_Click(object sender, EventArgs e)
        {           
            displayPickPullAuditReport(dtpPickListDate.Value);                  
        }

        private void displayPickPullAuditReport(DateTime PullDate)
        {
            string reportString = "";
            string jobNumList = "";
            string jobNumStr = "";
            bool firstJob = true;

            DataTable dt = GetOAU_PickListJobNumberByPullDate(PullDate);
            
            foreach(DataRow row in dt.Rows)
            {
                jobNumStr = row["JobNum"].ToString();                   
              
                if (firstJob)
                {
                    jobNumList = jobNumStr;
                    firstJob = false;
                }
                else
                {
                    jobNumList += ", " + jobNumStr;
                }                  
            }

            ReportDocument cryRpt = new ReportDocument();
            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@PullDate";
            pdv1.Value = PullDate;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@JobNumList";
            pdv2.Value = jobNumList;
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            frmReport frmRpt = new frmReport();

            frmRpt.crystalReportViewer1.ParameterFieldInfo = paramFields;

            EpicorServer = ConfigurationManager.AppSettings["EpicorAppServer"];
            reportString = @"\\" + EpicorServer + "\\Apps\\AMM_AuditReporting\\reports\\OAU_ProdPickPullAudit3.rpt";

            //reportString = @"\\kccwvpepic9app\Apps\AMM_AuditReporting\reports\OAU_ProdPickPullAudit3.rpt";           
#if DEBUG
            reportString = @"\\kccwvtepic9app2\Apps\AMM_AuditReporting\reports\OAU_ProdPickPullAudit3.rpt";              
#endif
          
            cryRpt.Load(reportString);

            frmRpt.crystalReportViewer1.ReportSource = cryRpt;
            frmRpt.ShowDialog();
            frmRpt.crystalReportViewer1.Refresh();

        }

        private DataTable GetOAU_PickListJobNumberByPullDate(DateTime PullDate)
        {            
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AMM_Reporting.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.GetOAU_PickListJobNumberByPullDate", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@PullDate", PullDate);                       
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;

        }    

        #endregion       

        #region AdjQtyAuditReport

        private void btnAdjQtyExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnAdjQtyDisplay_Click(object sender, EventArgs e)
        {
            string reportString = "";            
           
            ReportDocument cryRpt = new ReportDocument();

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@StartDate";
            pdv1.Value = Convert.ToDateTime(this.dtpAdjQtyStartDate.Text);
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@EndDate";
            pdv2.Value = Convert.ToDateTime(this.dtpAdjQtyEndDate.Text);
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);            

            this.crvAdjQtyAuditRpt.ParameterFieldInfo = paramFields;

            EpicorServer = ConfigurationManager.AppSettings["EpicorAppServer"];
            reportString = @"\\" + EpicorServer + "\\Apps\\AMM_AuditReporting\\reports\\OAU_ProdAdjQtyAudit.rpt";

            //reportString = @"\\kccwvpepic9app\Apps\AMM_AuditReporting\reports\OAU_ProdAdjQtyAudit.rpt";

#if DEBUG
            reportString = @"\\kccwvtepic9app2\Apps\AMM_AuditReporting\reports\OAU_ProdAdjQtyAudit.rpt";             
#endif

            cryRpt.Load(reportString);

            this.crvAdjQtyAuditRpt.ReportSource = cryRpt;
            this.crvAdjQtyAuditRpt.Visible = true;

            this.Refresh();           
        }
        #endregion

        #region NegativeQtyAuditReport

        private void btnNegQtyExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnNegQtyDisplay_Click(object sender, EventArgs e)
        {
            string reportString = "";

            ReportDocument cryRpt = new ReportDocument();


            EpicorServer = ConfigurationManager.AppSettings["EpicorAppServer"];
            reportString = @"\\" + EpicorServer + "\\Apps\\AMM_AuditReporting\\reports\\OAU_ProdPartBinNegativeQtyAudit.rpt";

            //reportString = @"\\kccwvpepic9app\Apps\AMM_AuditReporting\reports\OAU_ProdPartBinNegativeQtyAudit.rpt";

#if DEBUG
            reportString = @"\\kccwvtepic9app2\Apps\AMM_AuditReporting\reports\OAU_ProdPartBinNegativeQtyAudit.rpt";             
#endif

            cryRpt.Load(reportString);

            this.crvNegQtyRpt.ReportSource = cryRpt;
            this.crvNegQtyRpt.Visible = true;

            this.Refresh();
        }
        #endregion


        #region NonNettable
        private void btn_RB_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        

        private void btn_RB_DisplayRpt_Click(object sender, EventArgs e)
        {
            string reportString = "";
            string binString = "";
            bool firstBin = true;

            if (cb_RB_ELVMI.Checked == true)
            {
                binString = "ELVMI";
                firstBin = false;
            }

            if (cb_RB_L2AS.Checked == true)
            {
                if (firstBin == false)
                {
                    binString += ",";
                }
                binString += "L2AS";
                firstBin = false;
            }

            if (cb_RB_L2EL.Checked == true)
            {
                if (firstBin == false)
                {
                    binString += ",";
                }
                binString += "L2EL";
                firstBin = false;
            }

            if (cb_RB_L2MF.Checked == true)
            {
                if (firstBin == false)
                {
                    binString += ",";
                }
                binString += "L2MF";
                firstBin = false;
            }

            if (cb_RB_L1REC.Checked == true)
            {
                if (firstBin == false)
                {
                    binString += ",";
                }
                binString += "L1REC";
                firstBin = false;
            }

            if (cb_RB_LBIN2.Checked == true)
            {
                if (firstBin == false)
                {
                    binString += ",";
                }
                binString += "LBIN2";
                firstBin = false;
            }

            if (cb_RB_LBIN1.Checked == true)
            {
                if (firstBin == false)
                {
                    binString += ",";
                }
                binString += "LBIN1";
                firstBin = false;
            }

            if (cb_RB_L4REC.Checked == true)
            {
                if (firstBin == false)
                {
                    binString += ",";
                }
                binString += "L4REC";
                firstBin = false;
            }

            if (cb_RB_L5REC.Checked == true)
            {
                if (firstBin == false)
                {
                    binString += ",";
                }
                binString += "L5REC";
                firstBin = false;
            }

            ReportDocument cryRpt = new ReportDocument();
            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@BinStr";
            pdv1.Value = binString;
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            //ParameterField pf2 = new ParameterField();
            //ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            //pf2.Name = "@EndDate";
            //pdv2.Value = Convert.ToDateTime(this.dtpElectronEndDate.Text);
            //pf2.CurrentValues.Add(pdv2);

            //paramFields.Add(pf2);          


            EpicorServer = ConfigurationManager.AppSettings["EpicorAppServer"];
            reportString = @"\\" + EpicorServer + "\\Apps\\AMM_AuditReporting\\reports\\OAU_ProdPartBinAuditReport.rpt";

            //reportString = @"\\kccwvpepic9app\Apps\AMM_AuditReporting\reports\OAU_ProdPartBinAuditReport.rpt";
            crv_BinReport.ParameterFieldInfo = paramFields;
#if DEBUG
            reportString = @"\\kccwvtepic9app2\Apps\AMM_AuditReporting\reports\OAU_ProdPartBinAuditReport.rpt";
            //reportString = @"\\epicor905app\Apps\AMM_AuditReporting\reports\OAU_TestElectronAMMPartTransAuditByDate.rpt";             
#endif

            cryRpt.Load(reportString);

            crv_BinReport.ReportSource = cryRpt;
            crv_BinReport.Visible = true;

            this.Refresh();
            this.crv_BinReport.Refresh();  
        }
        #endregion       

        #region AllPartsOnBOM

        private void btnAPOBDisplayRpt_Click(object sender, EventArgs e)
        {
            string reportString = "";                      

            ReportDocument cryRpt = new ReportDocument();

            ParameterValues curPV = new ParameterValues();

            ParameterFields paramFields = new ParameterFields();

            ParameterField pf1 = new ParameterField();
            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
            pf1.Name = "@PartNum";
            pdv1.Value = this.txtAPOB_PartNumList.Text; 
            pf1.CurrentValues.Add(pdv1);

            paramFields.Add(pf1);

            ParameterField pf2 = new ParameterField();
            ParameterDiscreteValue pdv2 = new ParameterDiscreteValue();
            pf2.Name = "@PartNumStartRange";
            //pdv2.Value = null;
            pdv2.Value = txtAPOB_PartNumRangeStart.Text.ToUpper();
            pf2.CurrentValues.Add(pdv2);

            paramFields.Add(pf2);

            ParameterField pf3 = new ParameterField();
            ParameterDiscreteValue pdv3 = new ParameterDiscreteValue();
            pf3.Name = "@PartNumEndRange";
            //pdv3.Value = null;
            pdv3.Value = txtAPOB_PartNumRangeEnd.Text.ToUpper();
            pf3.CurrentValues.Add(pdv3);

            paramFields.Add(pf3);

            ParameterField pf4 = new ParameterField();
            ParameterDiscreteValue pdv4 = new ParameterDiscreteValue();
            pf4.Name = "@PartCategory";
            pdv4.Value = cbAPOB_PartCategory.SelectedValue;
            pf4.CurrentValues.Add(pdv4);

            paramFields.Add(pf4);

            ParameterField pf5 = new ParameterField();
            ParameterDiscreteValue pdv5 = new ParameterDiscreteValue();
            pf5.Name = "@PartClass";
            pdv5.Value = cbAPOB_PartClass.SelectedValue;
            pf5.CurrentValues.Add(pdv5);

            paramFields.Add(pf5);

            ParameterField pf6 = new ParameterField();
            ParameterDiscreteValue pdv6 = new ParameterDiscreteValue();
            pf6.Name = "@SearchWord";           
            pdv6.Value = txtAPOB_PartSearchWord.Text;
            pf6.CurrentValues.Add(pdv6);

            paramFields.Add(pf6);

            //ParameterField pf7 = new ParameterField();
            //ParameterDiscreteValue pdv7 = new ParameterDiscreteValue();
            //pf7.Name = "@ReferenceCat";
            //pdv7.Value = cbAPOB_RefCategory.SelectedValue;
            //pf7.CurrentValues.Add(pdv7);

            //paramFields.Add(pf7);

            ParameterField pf8 = new ParameterField();
            ParameterDiscreteValue pdv8 = new ParameterDiscreteValue();
            pf8.Name = "@BeginDate";
            pdv8.Value = Convert.ToDateTime(this.dtpAPOB_BeginDate.Value.ToShortDateString());
            pf8.CurrentValues.Add(pdv8);

            paramFields.Add(pf8);

            ParameterField pf9 = new ParameterField();
            ParameterDiscreteValue pdv9 = new ParameterDiscreteValue();
            pf9.Name = "@EndDate";
            pdv9.Value = Convert.ToDateTime(this.dtpAPOB_EndDate.Value.ToShortDateString());
            pf9.CurrentValues.Add(pdv9);

            paramFields.Add(pf9);

            ParameterField pf10 = new ParameterField();
            ParameterDiscreteValue pdv10 = new ParameterDiscreteValue();
            pf10.Name = "@SearchMode";
            pdv10.Value = searchMode;
            pf10.CurrentValues.Add(pdv10);

            paramFields.Add(pf10);

            crvAPOB_PurchParts.ParameterFieldInfo = paramFields;

            EpicorServer = ConfigurationManager.AppSettings["EpicorAppServer"];
            reportString = @"\\" + EpicorServer + "\\Apps\\AMM_AuditReporting\\reports\\OAU_ProdAPOBPurchasePartsStatus.rpt"; 

//            reportString = @"\\KCCWVTEPIC9APP2\Apps\AMM_AuditReporting\reports\OAU_ProdAPOBPurchasePartsStatus.rpt";
//#if DEBUG
//            reportString = @"\\KCCWVTEPIC9APP2\Apps\AMM_AuditReporting\reports\OAU_ProdAPOBPurchasePartsStatus.rpt";             
//#endif

            cryRpt.Load(reportString);

            crvAPOB_PurchParts.ReportSource = cryRpt;
            crvAPOB_PurchParts.Visible = true;

            this.Refresh();   
        }

        private void btnAPOB_Reset_Click(object sender, EventArgs e)
        {
            txtAPOB_PartNumList.Enabled = true;
            txtAPOB_PartNumList.Text= "";

            txtAPOB_PartNumRangeStart.Enabled = true;
            txtAPOB_PartNumRangeStart.Text = "";

            txtAPOB_PartNumRangeStart.Enabled = true;
            txtAPOB_PartNumRangeStart.Text = "";

            txtAPOB_PartNumRangeEnd.Enabled = true;
            txtAPOB_PartNumRangeEnd.Text = "";

            cbAPOB_PartCategory.Enabled = true;
            cbAPOB_PartCategory.SelectedIndex = 0;

            txtAPOB_PartSearchWord.Enabled = true;
            txtAPOB_PartSearchWord.Text = "";

            cbAPOB_PartClass.Enabled = true;
            cbAPOB_PartClass.SelectedIndex = 0;

            //cbAPOB_RefCategory.Enabled = false;
            //cbAPOB_RefCategory.SelectedIndex = 0;
        }

        private void btnAPOB_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        
        private void txtAPOB_PartNumList_TextChanged(object sender, EventArgs e)
        {
            if (txtAPOB_PartNumList.Text.Length > 0)
            {
                txtAPOB_PartNumRangeStart.Enabled = false;
                txtAPOB_PartNumRangeStart.Enabled = false;
                txtAPOB_PartNumRangeEnd.Enabled = false;                
                cbAPOB_PartCategory.Enabled = false;
                txtAPOB_PartSearchWord.Enabled = false;
                cbAPOB_PartClass.Enabled = false;
                cbAPOB_RefCategory.Enabled = false;
            }           
        }
              
        private void txtAPOB_PartNumRangeStart_TextChanged(object sender, EventArgs e)
        {
            searchMode = 2;
            if (txtAPOB_PartNumRangeStart.Text.Length > 0)
            {
                txtAPOB_PartNumList.Enabled = false;
                cbAPOB_PartCategory.Enabled = false;
                txtAPOB_PartSearchWord.Enabled = false;
                cbAPOB_PartClass.Enabled = false;
                cbAPOB_RefCategory.Enabled = false;
            }
        }
       
        private void txtAPOB_PartNumRangeEnd_TextChanged(object sender, EventArgs e)
        {
            searchMode = 2;
            if (txtAPOB_PartNumRangeEnd.Text.Length > 0)
            {
                txtAPOB_PartNumList.Enabled = false;
                cbAPOB_PartCategory.Enabled = false;
                txtAPOB_PartSearchWord.Enabled = false;
                cbAPOB_PartClass.Enabled = false;
                cbAPOB_RefCategory.Enabled = false;
            }
        }
       
        private void txtAPOB_PartSearchWord_TextChanged(object sender, EventArgs e)
        {
            searchMode = 5;
            if (txtAPOB_PartSearchWord.Text.Length > 0)
            {
                txtAPOB_PartNumList.Enabled = false;
                txtAPOB_PartNumRangeStart.Enabled = false;
                txtAPOB_PartNumRangeStart.Enabled = false;
                txtAPOB_PartNumRangeEnd.Enabled = false;
                cbAPOB_PartCategory.Enabled = false;
                cbAPOB_PartClass.Enabled = false;
                cbAPOB_RefCategory.Enabled = false;
            }           
        }
       
        private void cbAPOB_PartCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbAPOB_PartCategory.SelectedIndex != 0)
            {
                searchMode = 3;
                txtAPOB_PartNumList.Enabled = false;
                txtAPOB_PartNumRangeStart.Enabled = false;
                txtAPOB_PartNumRangeStart.Enabled = false;
                txtAPOB_PartNumRangeEnd.Enabled = false;
                txtAPOB_PartSearchWord.Enabled = false;
                cbAPOB_PartClass.Enabled = false;
                cbAPOB_RefCategory.Enabled = false;
            }
        }

        private void cbAPOB_RefCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtAPOB_PartNumList.Enabled = false;
            txtAPOB_PartNumRangeStart.Enabled = false;
            txtAPOB_PartNumRangeStart.Enabled = false;
            txtAPOB_PartNumRangeEnd.Enabled = false;
            txtAPOB_PartSearchWord.Enabled = false;
            cbAPOB_PartClass.Enabled = false;
            cbAPOB_PartCategory.Enabled = false;
        }
        
        private void cbAPOB_PartClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbAPOB_PartClass.SelectedIndex != 0)
            {
                searchMode = 4;
                txtAPOB_PartNumList.Enabled = false;
                txtAPOB_PartNumRangeStart.Enabled = false;
                txtAPOB_PartNumRangeStart.Enabled = false;
                txtAPOB_PartNumRangeEnd.Enabled = false;
                txtAPOB_PartSearchWord.Enabled = false;
                cbAPOB_PartCategory.Enabled = false;
                cbAPOB_RefCategory.Enabled = false;
            }
        }

        private void setupAPOB_Page()
        {            
            txtAPOB_PartNumList.Text = null;
            txtAPOB_PartNumRangeStart.Text = null;
            txtAPOB_PartNumRangeStart.Text = null;
            txtAPOB_PartNumRangeEnd.Text = null;

            DataTable dtPartCat = GetPartCategoryList();

            cbAPOB_PartCategory.DisplayMember = "CategoryName";
            cbAPOB_PartCategory.ValueMember = "CategoryName";
            cbAPOB_PartCategory.DataSource = dtPartCat;
            //cbAPOB_PartCategory.SelectedIndex = 0;

            DataTable dtClass = GetPartClassList();

            cbAPOB_PartClass.DisplayMember = "ClassDesc";
            cbAPOB_PartClass.ValueMember = "ClassID";
            cbAPOB_PartClass.DataSource = dtClass;
            //cbAPOB_PartClass.SelectedIndex = 0;
            
            txtAPOB_PartSearchWord.Text = null;  
          
            //cbAPOB_RefCategory.SelectedIndex = 0;
        }

        private DataTable GetPartCategoryList()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AMM_Reporting.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("[KCC].[R6_OA_GetPartCategoryList]", connection))
                    {
                        cmd.CommandTimeout = 120;
                        cmd.CommandType = CommandType.StoredProcedure;                     
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        private DataTable GetPartClassList()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AMM_Reporting.Properties.Settings.KCCConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("[KCC].[R6_OA_GetPartClassList]", connection))
                    {
                        cmd.CommandTimeout = 120;
                        cmd.CommandType = CommandType.StoredProcedure;
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

    }
}
