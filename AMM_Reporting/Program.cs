﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;

namespace AMM_Reporting
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

//#if DEBUG          
      
//            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
//            var connectionStringsSection = (ConnectionStringsSection)config.GetSection("connectionStrings");
//            connectionStringsSection.ConnectionStrings["AMM_Reporting.Properties.Settings.KCCConnectionString"].ConnectionString = "Data Source=EPICOR905dbtest;Initial Catalog=KCC;Integrated Security=True";
//            config.Save();
//            ConfigurationManager.RefreshSection("connectionStrings");
            
//            string testStr = connectionStringsSection.ConnectionStrings["AMM_Reporting.Properties.Settings.KCCConnectionString"].ConnectionString;
//#endif
            var configTest = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var connectionStringsSectionTest = (ConnectionStringsSection)configTest.GetSection("connectionStrings");
            string testStr2 = connectionStringsSectionTest.ConnectionStrings["AMM_Reporting.Properties.Settings.KCCConnectionString"].ConnectionString;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmAMM_Report());

        }
    }
}
