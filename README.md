# AMM_Reporting

This windows forms application is used by production supervisors to look at transactions that take place in the system to figure out if the right parts have been pulled and who pulled what.

## Project Structure

### AMM_Reporting
This project is a .NET Framework 4 windows application project.

## Data Access
All data access to the KCC database is done via ADO.NET and stored procedures, as well as a Visual Studio dataset. The following stored procedures are used:

* GetPurchasePartList
* GetUserList
* GetOAU_PickListMain
* GetOAU_PickListJobNumberByPullDate

## Reports
The following Crystal Reports are used (reports not stored on epicor905app server are used for debugging purposes):

* \\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdPartTransAuditTest.rpt
* \\epicor905app\Apps\AMM_AuditReporting\reports\OAU_TestPartTransAudit.rpt
* \\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdIndividualPartTransAudit.rpt
* \\epicor905app\Apps\AMM_AuditReporting\reports\OAU_TestIndividualPartTransAudit.rpt
* \\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdIndividualUserTransAudit.rpt
* \\epicor905app\Apps\AMM_AuditReporting\reports\OAU_TestIndividualUserTransAudit.rpt
* \\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdElectronAMMPartTransAuditByDate.rpt
* \\epicor905app\Apps\AMM_AuditReporting\reports\OAU_TestElectronAMMPartTransAuditByDate.rpt
* \\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdElectronAMMPartTransAudit.rpt
* \\epicor905app\Apps\AMM_AuditReporting\reports\OAU_TestElectronAMMPartTransAudit.rpt
* \\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdPickPullAudit3.rpt
* F:\Projects\AMM_Reporting\AMM_Reporting\reports\OAU_ProdPickPullAudit3.rpt
* \\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdAdjQtyAudit.rpt
* \\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdPartBinNegativeQtyAudit.rpt
* \\epicor905app\Apps\AMM_AuditReporting\reports\OAU_ProdPartBinAuditReport.rpt

## How to run locally
Assuming a VPN connection, make sure solution is ready to run in Debug configuration. Change the connection strings in App.config to point to the appropriate local or dev KCC database.
Make sure Crystal Reports for Visual Studio is installed, and fix all broken references to point to the installed Crystal Reports .dlls. Build the solution and run it. 

## How to deploy
One-click publish: [https://docs.microsoft.com/en-us/visualstudio/deployment/how-to-publish-a-clickonce-application-using-the-publish-wizard?view=vs-2019]

## Author/Devs
Tony Thoman